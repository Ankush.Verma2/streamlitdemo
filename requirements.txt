# pip install -r requirements
#-r https://raw.githubusercontent.com/snowflakedb/snowflake-connector-python/v2.7.9/tested_requirements/requirements_39.reqs
altair==4.2.0
impyla==0.17.0
matplotlib==3.5.3
pandas==1.4.3
Pillow==9.2.0
pip==22.2
pretty-confusion-matrix==0.1.1
pure-sasl==0.6.2
pyodbc==4.0.34
pyarrow==8.0.0
snowflake-connector-python==2.7.12
snowflake-snowpark-python==0.8.0
streamlit==1.10.0
streamlit-aggrid==0.3.2
thrift==0.11.0
thrift-sasl==0.4.3
sqlalchemy==1.4.40
vega-datasets==0.9.0
html2image==2.0.1