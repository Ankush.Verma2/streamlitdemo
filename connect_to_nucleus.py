from re import I
import snowflake
import snowflake.connector 
import snowflake.connector.pandas_tools
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
import os

usr = os.getenv('ZID')
passcd = os.getenv('ID_RSA_PASS').encode()
pkey   = os.getenv('ID_RSA_P8').replace('|','\n').encode()

p_key = serialization.load_pem_private_key(
  pkey
  ,password = passcd
  ,backend=default_backend()
)
pkb = p_key.private_bytes(
  encoding=serialization.Encoding.DER,
  format=serialization.PrivateFormat.PKCS8,
  encryption_algorithm=serialization.NoEncryption(),
)

conn = snowflake.connector.connect(
  user = usr
  ,account = 'ally.us-east-1.privatelink'
  ,warehouse = 'WH_GENERAL_XS'
  ,database = 'TEAM_TECH_MEAD_P'
  ,schema = 'CORE'
  ,role = 'RL_TEAM_TECH_MEAD_P'
  ,private_key=pkb
)

