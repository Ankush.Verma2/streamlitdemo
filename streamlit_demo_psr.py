import pandas as pd
import streamlit as st
import scipy
import plotly.figure_factory as ff
import numpy as np
from datetime import datetime

##############################################################
## STREAMLIT PAGE HEADER

#Page setup in browswer bar
st.set_page_config(page_title='Streamlit Demo - Ally EDA',  layout='wide')


#Section - Images/Title
a1, a2, a3, a4 = st.columns((2,5,1,1))
a1.image('images/ally.png', width = 150)
a2.title("Nucleus Query Dashboard")
a3.image('images/bee 5.png', width = 100)
a4.image('images/Nucleus 5.png', width = 100)

##############################################################
## QUERY NUCLEUS
import connect_to_nucleus

cur = connect_to_nucleus.conn.cursor()
cur.execute("""
select * from team_tech_mead_p.core.rpt_psr1
""")
df_techkpi = cur.fetch_pandas_all()

cur.execute("""
select * from team_tech_mead_p.core.rpt_psr2
""")
df_usr_wh = cur.fetch_pandas_all()

df_qry_ct_by_usr_type = df_techkpi[["QRY_DATE","QRY_CT","QRY_COST"]].sort_values(by="QRY_DATE")
#st.write(df_qry_ct_by_usr_type.head())
df_qry_ct_by_usr_type = df_qry_ct_by_usr_type.groupby(["QRY_DATE"], dropna=True).sum().sort_values(by="QRY_DATE")
df_qry_ct_by_usr_type = df_qry_ct_by_usr_type.reset_index(level=["QRY_DATE"])
#st.write(df_qry_ct_by_usr_type.head())

##############################################################
## COMMENT EXPANDER
cur.execute("""
select * from team_tech_mead_p.core.rpt_psr_comments
""")
df_comments = cur.fetch_pandas_all()
with st.expander("Page Comments:"):
    df_comments = df_comments.sort_values(by="COMMENT_TS",ascending=False)
    st.write(df_comments)
    st.write("""
        If you have a commment to be added to this page, submit it below.
        All comments will be stored in the database and shown on this page.
    """)
    form = st.form("comment")
    name = form.text_input("Name")
    comment = form.text_area("Comment")
    submit = form.form_submit_button("Add comment")
    if submit:
        date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        cur.execute(f"""
            insert into team_tech_mead_p.core.rpt_psr_comments
            values ('{date}', '{name}', '{comment}')
        """)
        if "just_posted" not in st.session_state:
            st.session_state["just_posted"] = True
        st.experimental_rerun()


##############################################################
## ROW OF CHARTS
from datetime import datetime
import streamlit as st
from vega_datasets import data
from utils import chart
pip
#THIS IS THE SNOWFLAKE DATA
df_ct_by_usr_type = df_techkpi.groupby(["QRY_DATE2","USR_TYPE"]).sum().sort_values(by="QRY_CT",ascending=False)
df_ct_by_usr_type = df_ct_by_usr_type.reset_index(level=["QRY_DATE2","USR_TYPE"])
df_ct_by_usr_type = df_ct_by_usr_type.rename(columns={"QRY_DATE2":"date", "USR_TYPE": "user_type", "QRY_CT": "qry_ct"})
#st.write(df_for_type_agg_table.head())
df_cost_by_usr_type = df_techkpi.groupby(["QRY_DATE2","USR_TYPE"]).sum().sort_values(by="QRY_COST",ascending=False)
df_cost_by_usr_type = df_cost_by_usr_type.reset_index(level=["QRY_DATE2","USR_TYPE"])
df_cost_by_usr_type = df_cost_by_usr_type.rename(columns={"QRY_DATE2":"date", "USR_TYPE": "user_type", "QRY_COST": "qry_cost"})


all_types_ct = df_ct_by_usr_type["user_type"].unique()
user_types = st.multiselect("Choose stocks to visualize", all_types_ct, all_types_ct[:6])
source_ct = df_ct_by_usr_type[df_ct_by_usr_type["user_type"].isin(user_types)]
source_cost = df_cost_by_usr_type[df_cost_by_usr_type["user_type"].isin(user_types)]


col1, col2 = st.columns((1,1))
chart_ct = chart.get_chart(source_ct, title="Daily Query Count by User Type",x="date",y="qry_ct",symbol="user_type")
col1.altair_chart(chart_ct, use_container_width=True)
chart_cost = chart.get_chart(source_cost, title="Daily Query Cost by User Type",x="date",y="qry_cost",symbol="user_type")
col2.altair_chart(chart_cost, use_container_width=True)



##############################################################
## INTERACTIVE TABLE
df_for_user_agg_table = df_techkpi.groupby(["USR_TYPE","USR_NAME","USR_FULL_NAME","USR_LDR_NAME"]).sum().sort_values(by="QRY_CT",ascending=False)
df_for_user_agg_table = df_for_user_agg_table.reset_index(level=["USR_TYPE","USR_NAME","USR_FULL_NAME","USR_LDR_NAME"])
#st.write(df_for_user_agg_table.head())

try:
    selection = chart.aggrid_interactive_table(df=df_for_user_agg_table[df_for_user_agg_table["USR_TYPE"].isin(user_types)])    
except:
    st.write("")

##############################################################
## INTERACTIVE TABLE
import altair as alt
from vega_datasets import data

# Bar Chart - Count & Cost by Warehouse
bar_chart_source = data.barley()
bar_chart = (alt.Chart(bar_chart_source).mark_bar().encode(
    x='year:O',
    y='sum(yield):Q',
    color='year:N',
    column='site:N'
)).interactive()

# Line Chart - Count by Warehouse
line_chart_ct_source = df_usr_wh.groupby(["QRY_DATE2","USR_NAME","QRY_WH"]).sum().sort_values(by="QRY_CT",ascending=False)
line_chart_ct_source = line_chart_ct_source.reset_index(level=["QRY_DATE2","USR_NAME","QRY_WH"])
line_chart_ct_source = line_chart_ct_source.rename(columns={"QRY_DATE2":"week", "USR_NAME": "user", "QRY_WH": "warehouse", "QRY_CT": "qry_ct"})

# Line Chart - Cost by Warehouse
line_chart_cost_source = df_usr_wh.groupby(["QRY_DATE2","USR_NAME","QRY_WH"]).sum().sort_values(by="QRY_COST",ascending=False)
line_chart_cost_source = line_chart_cost_source.reset_index(level=["QRY_DATE2","USR_NAME","QRY_WH"])
line_chart_cost_source = line_chart_cost_source.rename(columns={"QRY_DATE2":"week", "USR_NAME": "user", "QRY_WH": "warehouse", "QRY_COST": "qry_cost"})

st.write("Click on a row in the table to see detailed charts here...")
col1, col2 = st.columns((1,1))
if selection:
    try:
        selected_user = selection["selected_rows"][0]["USR_NAME"]
        selected_name = selection["selected_rows"][0]["USR_FULL_NAME"]
        line_chart_ct_by_wh = chart.get_chart(
            line_chart_ct_source[line_chart_ct_source["user"] == selected_user],
            title="Query Count By Warehouse - " + selected_name,
            x="week",
            y="qry_ct",
            symbol="warehouse"
        )
        line_chart_cost_by_wh = chart.get_chart(
            line_chart_cost_source[line_chart_cost_source["user"] == selected_user],
            title="Query Cost By Warehouse - " + selected_name,
            x="week",
            y="qry_cost",
            symbol="warehouse"
        )
        col1.altair_chart(line_chart_ct_by_wh,use_container_width=True)
        col2.altair_chart(line_chart_cost_by_wh,use_container_width=True)
    except:
        st.write("...no selection yet")
